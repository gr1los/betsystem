#include <map>
#include <vector>
#include "./Match/Match.h"
#include "./Bet/Bet.h"
#include "./Result/Result.h"
#include "./Collect/Collect.h"

using namespace std;

int tmp_id;
string tmp_betid;
bool insertMode = true;

map<int,Match> matches;
map<string,Bet> bets;

Match match;
Bet bet;
Result results;

template<typename T>
vector<T>& operator,(T& a, T& b){

	vector<T>* vec = new vector<T>;

	vec->push_back(a);
	vec->push_back(b);

	return *vec;
}

template<typename T>

vector<T>& operator,(vector<T>& a, T& b){
	a.push_back(b);

	return a;
}


Match& addToMap(int match_id){
	tmp_id = match_id;
	map<int,Match>::iterator it;

	it = matches.find(match_id);

	if( it==matches.end()) {	// if not found add it
		if(!insertMode){		// if it's invalid change the flag
			bets[tmp_betid].setInvalid();
		}
		matches[match_id] = Match(match_id);
		return matches[match_id];
	}

	return ((*it).second);

}

Bet& addToMap(string bet_id){
	tmp_betid = bet_id;
	map<string,Bet>::iterator it;

	it = bets.find(bet_id);

	if( it==bets.end() ) {	// if not found add it
		bets[bet_id] = Bet(bet_id);
		return bets[bet_id];
	}

	return ((*it).second);
}

void checkBets(string bet_id){
	vector<PartialBet> vec = bets[bet_id].getBets();
	int maxCombo = 0;

	for (int i=0 ; i<vec.size() ; ++i){
		if(matches[vec[i].getMatchId()].getCombo() > maxCombo){
			maxCombo = matches[vec[i].getMatchId()].getCombo();
		}
	}
	if(maxCombo != vec.size()){
		bets[bet_id].setInvalid();
	}
}

#define MATCH(match_id) addToMap(match_id)-

#define TEAM(name) #name

#define COMBINATION(combo) ;addToMap(tmp_id).setCombo(combo);

#define ODDS addToMap(tmp_id),

#define HOMEWIN	PartialOdd('h')

#define DRAW PartialOdd('d')

#define AWAYWIN PartialOdd('a')

#define FULLTIME_RESULT	Odd('f')

#define HALFTIME_RESULT Odd('h')

#define UNDER Odd('u')

#define OVER Odd('o')

#define BET(bet_id) insertMode=false; Bet *bet_id=&addToMap(#bet_id); (*bet_id)

#define AMOUNT(value) ;checkBets(tmp_betid);addToMap(tmp_betid).setAmount(value)

#define CANCELLED PartialResult().setCanceled()

#define RESULTS results

#define COLLECT Collect(&matches,&results),


