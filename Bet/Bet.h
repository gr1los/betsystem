
#ifndef BET_H_
#define BET_H_

#include "PartialBet.h"
#include <vector>
#include <iostream>

using namespace std;

class Bet {
public:
	Bet();
	Bet(string);
	virtual ~Bet();

	Bet& operator[](const vector<PartialBet>&);
	Bet& operator[](const PartialBet&);
	vector<PartialBet> getBets() const;

	void setAmount(int amount);
	void setInvalid();

	string getName() const;
	int getAmount() const;
	bool isValid() const;

	void showData() const;

private:
	string name;
	int amount;
	bool valid;
	vector<PartialBet> bets;
};

#endif /* BET_H_ */
