
#include "Bet.h"

using namespace std;

Bet::Bet() {}

Bet::Bet(string name) : name(name) , valid(true){}

Bet::~Bet() {}

Bet& Bet::operator [](const vector<PartialBet>& vec){
	for(int i=0; i< vec.size() ; ++i){
		bets.push_back(vec[i]);
	}
	return *this;
}

Bet& Bet::operator [](const PartialBet& pbet){
	bets.push_back(pbet);
	return *this;
}

vector<PartialBet> Bet::getBets() const { return this->bets;}

void Bet::setAmount(int amount) { this->amount = amount; }

string Bet::getName() const{
	return this->name;
}

int Bet::getAmount() const {
	return this->amount;
}

void Bet::setInvalid(){ this ->valid = false; }

bool Bet::isValid() const { return this->valid; }

void Bet::showData() const{
	cout << "Bet: " << this->name << endl;
	for(int i=0 ; i < this->bets.size() ; ++i){
		cout << '\t' << "MatchID :" << this->bets[i].getMatchId() << '\t';
		if(this->bets[i].isHalf()){
			cout << "Half -> ";
			cout << this->bets[i].getType() << endl;
		}else{
			if(this->bets[i].getType() == 'o'){
				cout << "Over" << endl;
			}else if(this->bets[i].getType() == 'u'){
				cout << "Under" << endl ;
			}else{
				cout << "Full -> ";
				cout << this->bets[i].getType() << endl;
			}
		}
	}
	cout << "Amount: " << this->amount << endl;
}
