
#ifndef PARTIALBET_H_
#define PARTIALBET_H_

#include "../Result/Result.h"

class PartialBet {
public:
	PartialBet();
	PartialBet(int,bool,char);
	virtual ~PartialBet();

	int getMatchId() const;
	bool isHalf() const;
	char getType() const;

	bool Evaluate(const Result&);
	bool halfTimeEvaluate(const Result&);
	bool fullTimeEvaluate(const Result&);

private:

	int match_id;
	bool half;
	char type;
};

#endif /* PARTIALBET_H_ */
