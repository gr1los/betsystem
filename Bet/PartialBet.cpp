
#include "PartialBet.h"

PartialBet::PartialBet() {}

PartialBet::PartialBet(int match_id, bool half, char type)
	: match_id(match_id) , half(half) , type(type) {}


PartialBet::~PartialBet() {}

int PartialBet::getMatchId() const { return this->match_id; }

bool PartialBet::isHalf() const { return this->half; }

char PartialBet::getType() const { return this->type; }

bool PartialBet::halfTimeEvaluate(const Result& res){
	for(int i=0; i< res.getResults().size() ; ++i){
		if(res.getResults().at(i).getMatchId() == this->getMatchId()){
			if(res.getResults().at(i).isCanceled())
				return true;
			if(this->getType() == res.getResults().at(i).getHalf()){
				return true;
			}
			return false;
		}
	}
	return false;
}

bool PartialBet::fullTimeEvaluate(const Result& res){
	for(int i=0; i< res.getResults().size() ; ++i){
		if(res.getResults().at(i).getMatchId() == this->getMatchId()){
			if(res.getResults().at(i).isCanceled())
				return true;

			if(this->getType() == 'u' && !res.getResults().at(i).isOver()){
				return true;
			}

			if(this->getType() == 'o' && res.getResults().at(i).isOver() ){
				return true;
			}

			if(this->getType() == res.getResults().at(i).getFull()){
				return true;
			}
			return false;
		}
	}
	return false;
}

bool PartialBet::Evaluate(const Result& res){
	if(this->isHalf()){
		return this->halfTimeEvaluate(res);
	}else{
		return this->fullTimeEvaluate(res);
	}
}
