# betSystem

### What does it do?
* Special purposed language
* Uses Macros, Operator overloading to achieve the functionalities

### How to build?
* Install Dependencies (are included in every Debian's based distro package list)
    * g++ >= 4:5.2.1
    * make >= 4.0-8.2
* Clone this project
* cd betsystem/
* make

### How to use?
* Clone this project
* cd betsystem/
* Build the project
* ./bet.out

### Makefile rules:
* make debug
* make def
* make clean

**Note:** The main.cpp file includes an example for the language.