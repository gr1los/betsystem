CC=g++
FILES = ./Match/Match.cpp			\
		./Odd/Odd.cpp				\
		./Odd/PartialOdd.cpp		\
		./Bet/PartialBet.cpp		\
		./Bet/Bet.cpp				\
		./Result/Result.cpp			\
		./Result/PartialResult.cpp	\
		./Collect/Collect.cpp		\
		main.cpp

all:
	$(CC) $(FILES) -o bet.out

debug:
	$(CC) -g $(FILES) -o bet_debug.out

def:
	$(CC) -E $(FILES) > def.cpp

clean:
	-rm -f bet.out
	-rm -f bet_debug.out
