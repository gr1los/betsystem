
#ifndef MATCH_H_
#define MATCH_H_

#include "../Odd/Odd.h"
#include "../Result/PartialResult.h"
#include "../Bet/PartialBet.h"
#include <vector>

using namespace std;

class Match {
public:
	Match();
	Match(int);
	virtual ~Match();

	void setCombo(int);
	Match& operator-(const string);
	PartialBet& operator-(const Odd&);
	PartialResult& operator-(PartialResult&);
	Match& operator,(const Odd&);

	int getMatchId() const;
	int getCombo() const;
	string getTeam1() const;
	string getTeam2() const;
	float getFull(int) const;
	float getHalf(int) const;
	float getOver() const;
	float getUnder() const;

	void showData();

private:
	int match_id, combo;
	string team1, team2;
	Odd full, half;
	Odd over, under;

};

#endif /* MATCH_H_ */
