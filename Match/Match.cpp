
#include "Match.h"

using namespace std;

Match::Match() {}

Match::Match(int match_id){
	this->match_id = match_id;
	this->team1 = "";
	this->team2 = "";
	this->full = Odd('f');
	this->half = Odd('h');
	this->over = Odd('o');
	this->under = Odd('u');
}

Match::~Match() {}

void Match::setCombo(int combo){
	this->combo = combo;
}

Match& Match::operator -(const string str){
	if(this->team1==""){
		this->team1 = str;
	}else{
		this->team2 = str;
	}
	return *this;
}

PartialBet& Match::operator -(const Odd& odd){
	PartialBet *pbet;
	if(odd.getType() == 'f' || odd.getType() == 'o' || odd.getType() == 'u'){
		pbet = new PartialBet(this->match_id,false,odd.getPartialVec().at(0).getType());
	}else{
		pbet = new PartialBet(this->match_id,true,odd.getPartialVec().at(0).getType());
	}
	return *pbet;
}

PartialResult& Match::operator -(PartialResult& pres){
	pres.setMatchId(this->match_id);
	return pres;
}

Match& Match::operator ,(const Odd& odds) {

	switch(odds.getType()){
	case 'f' :
		this->full = odds;
		break;
	case 'h' :
		this->half = odds;
		break;
	case 'o' :
		this->over = odds;
		break;
	case 'u' :
		this->under = odds;
		break;
	}
	return *this;
}

int Match::getMatchId() const{ return this->match_id ;}

int Match::getCombo() const{ return this->combo; }

string Match::getTeam1() const{ return this->team1; }

string Match::getTeam2() const{ return this->team2; }


float Match::getFull(int index) const {
	return this->full.getPartialVec().at(index).getValue();
}

float Match::getHalf(int index) const {
	return this->half.getPartialVec().at(index).getValue();
}

float Match::getOver() const {
	return this->over.getPartialVec().at(0).getValue();
}

float Match::getUnder() const {
	return this->under.getPartialVec().at(0).getValue();
}

void Match::showData() {
	vector<PartialOdd> vec;
	cout << endl << getMatchId() << '\t';
	cout << getTeam1()<< " - ";
	cout << getTeam2()<< '\t';
	cout << "Combo : " << getCombo() << endl;
	cout << '\t' << "Full: ";
	vec = this->full.getPartialVec();
	for(int i=0 ; i< vec.size(); ++i){
		cout << vec[i].getValue() << '\t' ;
	}
	cout << endl << '\t' << "Half: ";
	vec = this->half.getPartialVec();
	for(int i=0 ; i< vec.size(); ++i){
		cout << vec[i].getValue() << '\t' ;
	}

	cout <<  endl << '\t' << "Under: ";
	vec = this->under.getPartialVec();
	cout << vec[0].getValue() << '\t' ;

	cout << "Over: ";
	vec = this->over.getPartialVec();
	cout << vec[0].getValue() << endl ;


}

