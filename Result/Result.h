
#ifndef RESULT_H_
#define RESULT_H_

#include "PartialResult.h"
#include <vector>
#include <iostream>

using namespace std;

class Result {
public:
	Result();
	virtual ~Result();

	void operator[](const vector<PartialResult>&);
	void showData() const;

	vector<PartialResult> getResults() const ;

private:
	vector<PartialResult> results;
};

#endif /* RESULT_H_ */
