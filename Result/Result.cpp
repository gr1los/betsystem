
#include "Result.h"

using namespace std;

Result::Result() {}

Result::~Result() {}

void Result::operator[](const vector<PartialResult>& res){
	this->results = res;
}

vector<PartialResult> Result::getResults() const{
	return this->results;
}

void Result::showData() const {

	for(int i=0 ;i<results.size();++i){
		cout << "Match Id: " << results[i].getMatchId() << endl;
		if(results[i].isCanceled()){
			cout << "Cancelled." << endl;
		}else{
			cout << "Half: " << results[i].getHalf() << endl;
			cout << "Full: " << results[i].getFull() << endl;
			if(results[i].isOver()){
				cout << "Over." << endl;
			}else{
				cout << "Under." << endl;
			}
		}
	}
}
