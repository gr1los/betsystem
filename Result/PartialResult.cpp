
#include "PartialResult.h"

PartialResult::PartialResult() {}

PartialResult::PartialResult(bool isOver, char half, char full) :
		over(isOver) , half(half) , full(full) , canceled(false){}

PartialResult::~PartialResult() {}

PartialResult& PartialResult::operator /(const PartialResult& pres){
	this->full = pres.getFull();
	this->over = pres.isOver();
	return *this;
}

int PartialResult::getMatchId() const { return this->match_id; }

char PartialResult::getHalf() const { return this->half; }

char PartialResult::getFull() const { return this->full; }

bool PartialResult::isCanceled() const { return this->canceled; }

bool PartialResult::isOver() const { return this->over; }

void PartialResult::setMatchId(const int id){
	this->match_id = id;
}

PartialResult& PartialResult::setCanceled(){
	this->canceled = true;
	return *this;
}
