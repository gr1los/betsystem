
#ifndef PARTIALRESULT_H_
#define PARTIALRESULT_H_

class PartialResult {
public:
	PartialResult();
	PartialResult(bool,char,char);
	virtual ~PartialResult();

	PartialResult& operator/(const PartialResult&);

	int getMatchId() const;
	char getHalf() const;
	char getFull() const;
	bool isCanceled() const;
	bool isOver() const;

	void setMatchId(const int);
	PartialResult& setCanceled();

private:
	int match_id;
	bool canceled, over;
	char half,full;
};

#endif /* PARTIALRESULT_H_ */
