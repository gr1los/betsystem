
#include "PartialOdd.h"

using namespace std;

PartialOdd::PartialOdd() : value(1) {}

PartialOdd::PartialOdd(char type) : type(type) , value(1){}

PartialOdd::~PartialOdd() {}

float PartialOdd::getValue() const { return this->value; }

char PartialOdd::getType() const { return this->type; }

PartialOdd& PartialOdd::operator =(const float value) {
	this->value = value;
	return *this;
}
