
#ifndef ODD_H_
#define ODD_H_

#include "../Odd/PartialOdd.h"
#include "../Result/PartialResult.h"
#include <vector>

class Odd {
public:
	Odd();
	Odd(char);
	virtual ~Odd();

	Odd& operator[](const vector<PartialOdd>&);
	Odd& operator=(float);
	Odd& operator()(const PartialOdd&);
	PartialResult& operator()(int,int);

	vector<PartialOdd> getPartialVec() const;
	char getType() const;
private:
	vector<PartialOdd> odds;
	char type;
};

#endif /* ODD_H_ */
