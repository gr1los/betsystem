
#ifndef PARTIALODD_H_
#define PARTIALODD_H_

#include <iostream>

using namespace std;

class PartialOdd {
public:
	PartialOdd();
	PartialOdd(char);
	virtual ~PartialOdd();

	float getValue()const;
	char getType()const;

	PartialOdd& operator=(const float);

private:
	float value;
	char type;
};

#endif /* PARTIALODD_H_ */
