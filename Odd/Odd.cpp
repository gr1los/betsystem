
#include "Odd.h"

using namespace std;

Odd::Odd(){}

Odd::Odd(char ch){

	if(ch == 'f' || ch == 'h'){
		this->odds.push_back(PartialOdd('h'));
		this->odds.push_back(PartialOdd('d'));
		this->odds.push_back(PartialOdd('a'));
	}else{	// if over or under
		this->odds.push_back(PartialOdd(ch));
	}
	this->type = ch;
}

Odd::~Odd() {}

Odd& Odd::operator [](const vector<PartialOdd>& vec) {
	this->odds = vec;
	return *this;
}

Odd& Odd::operator =(float f){
	this->odds[0]=f;
	return *this;
}

vector<PartialOdd> Odd::getPartialVec() const{
	return this->odds;
}

Odd& Odd::operator ()(const PartialOdd& podd) {
	this->odds[0] = podd;
	return *this;

}

PartialResult& Odd::operator ()(int arg1, int arg2){
	PartialResult *tmp;
	bool isOver = false;
	char half,full;

	if(this->type == 'f'){
		if(arg1 + arg2 >= 3)
				isOver=true;

		if(arg1 > arg2){
			full = 'h';
		}else if (arg1 < arg2){
			full = 'a';
		}else{
			full = 'd';
		}

	}else{

		if(arg1 > arg2){
			half = 'h';
		}else if (arg1 < arg2){
			half = 'a';
		}else{
			half = 'd';
		}
	}
	tmp = new PartialResult(isOver,half,full);
	return *tmp;
}


char Odd::getType() const {
	return this->type;
}
