
#ifndef COLLECT_H_
#define COLLECT_H_

#include "../Match/Match.h"
#include "../Result/Result.h"
#include "../Bet/Bet.h"
#include <map>

using namespace std;

class Collect {
public:
	Collect();
	Collect(map<int,Match>* , Result*);
	bool isCanceled(int);
	void print() const;
	virtual ~Collect();

	Collect& operator,(Bet*);

private:
	map<int,Match>* matches;
	Result* results;
	Bet* tmpBet;
	float odd;
	bool win;
};

#endif /* COLLECT_H_ */
