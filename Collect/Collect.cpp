
#include "Collect.h"

using namespace std;

Collect::Collect() {}

Collect::Collect(map<int, Match>* matches, Result* res) :
		matches(matches) , results(res){}

bool Collect::isCanceled(int match_id){
	for(int i = 0 ; i < results->getResults().size() ; ++i){
		if(results->getResults().at(i).getMatchId() ==  match_id){
			return results->getResults().at(i).isCanceled();
		}
	}
	return false;
}

Collect::~Collect() {}

void Collect::print() const{
	if(this->tmpBet->isValid()){
		cout << "Bet with id " << this->tmpBet->getName();
		if(this->win){
			cout << " wins " << odd * this->tmpBet->getAmount() << " euros." << endl;
		}else{
			cout << " loses " << endl;
		}
	}else{
		cout << "Cannot collect invalid bet " << this->tmpBet->getName() << endl;
	}
}

Collect& Collect::operator ,(Bet* bet) {
	this->tmpBet = bet;
	this->win= true;
	this->odd = 1;

	if(!bet->isValid()){this->print(); return *this; }

	for(int i=0 ;i < bet->getBets().size() ; ++i){
		PartialBet currBet = bet->getBets().at(i);
		Match currMatch = (*matches)[currBet.getMatchId()];

		win &= currBet.Evaluate(*results);

		if(isCanceled(currMatch.getMatchId())){
			odd *= 1;
			continue;
		}

		switch (currBet.getType()) {
		case 'h':
			if(currBet.isHalf()){
				this->odd *= currMatch.getHalf(0);
			}else{
				this->odd *= currMatch.getFull(0);
			}
			break;

		case 'd':
			if(currBet.isHalf()){
				this->odd *= currMatch.getHalf(1);
			}else{
				this->odd *= currMatch.getFull(1);
			}
			break;

		case 'a':
			if(currBet.isHalf()){
				this->odd *= currMatch.getHalf(2);
			}else{
				this->odd *= currMatch.getFull(2);
			}
			break;

		case 'o':
			this->odd *= currMatch.getOver();
			break;
		case 'u':
			this->odd *= currMatch.getUnder();
			break;
		}
	}
	this->print();
	return *this;
}
